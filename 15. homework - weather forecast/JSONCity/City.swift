

import Foundation

class City: Decodable {
    var id: Int
    var name: String
    var state: String
    var country: String
    var coord: Coord
}

class Coord: Decodable {
    var lon: Double
    var lat: Double
}
