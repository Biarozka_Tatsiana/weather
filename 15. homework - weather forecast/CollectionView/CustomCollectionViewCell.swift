

import UIKit


class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var hourlyLabel: UILabel!
    @IBOutlet weak var tempHourlyLabel: UILabel!
    @IBOutlet weak var hourlyImageView: UIImageView!
    
    var hourlyArray = [Hourly]()
    
    func configure(object: Hourly) {
        
        hourlyLabel.text = String(getHourDay(hourly: object))
        tempHourlyLabel.text = String(Int(object.temp.rounded()))
        hourlyImageView.image = UIImage(named: object.weather[0].icon)
        contentView.backgroundColor = .clear
    }
    
    func getHourDay (hourly: Hourly) -> String {
        
        let date = Date(timeIntervalSince1970: Double(hourly.dt))
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .none
        var string = dateFormatter.string(from: date)
        if string.last == "0" {
            string = String(string.prefix(string.count - 3))
        }
        return string
    }
}
