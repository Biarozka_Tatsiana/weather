

import UIKit
import CoreLocation
import SpriteKit
import RxCocoa
import RxSwift


class ViewController: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var blur: UIVisualEffectView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var separatorOne: UIView!
    @IBOutlet weak var separatorTwo: UIView!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var snowView: SKView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel: ViewModel!
    var dailyArray = [Daily]()
    var hourlyArray = [Hourly]()
    var current: Current?
    var widthCell = 50
    var numberOfSections = 2
    var rowSectionFirstIndex = 4
    let heightSecondSection: CGFloat = 80
    let heightRestSection: CGFloat = 44
    let disposeBag = DisposeBag()
    
    private let completeSceneSnow: CompletionScene! = SKScene(fileNamed: "CompletionScene") as? CompletionScene
    private let completeSceneRain: CompletionSceneRain! = SKScene(fileNamed: "CompletionSceneRain") as? CompletionSceneRain
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = ViewModel()
        setImageView()
        imageView.contentMode = .center
        imageView.addParalaxEffect()
        blur.isHidden = true
        view.insertSubview(mainView, belowSubview: blur)
        myTableView.tableFooterView = UIView()
        navigationController?.isNavigationBarHidden = true
        viewModel.getWeather(city: nil)
        bindView()
    }
    
    func setImageView (){
        let imageViewDay = UIImage(named: "background")
        let imageViewNight = UIImage(named: "cloud22")
        let hour = Calendar.current.component(.hour, from: Date())
        print(hour)
        switch hour {
        case 1...6:
            imageView.image = imageViewNight
        case 7...18:
            imageView.image = imageViewDay
        default:
            imageView.image = imageViewNight
        }
    }
    
    func getWeatherMain (currentWeather: Current, cityCurrent: String) {
        cityLabel.text = cityCurrent
        descriptionLabel.text = currentWeather.weather[0].description
        tempLabel.text = "\(Int(currentWeather.temp.rounded()))°"
        dateLabel.text = getTodayDate()
    }
    
    func getTodayDate () -> String {
        let today = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let string = formatter.string(from: today)
        return string
    }
    
    func bindView () {
        viewModel.resultRX.subscribe({ (result) in
            if let result = result.element, let element = result {
                self.getWeatherMain(currentWeather: element.current, cityCurrent: self.viewModel.cityCurrent)
            }
            guard let dailyArray = self.viewModel.resultRX.value?.daily else
            {return}
            self.dailyArray = dailyArray
            guard let hourlyArray = self.viewModel.resultRX.value?.hourly else {return}
            self.hourlyArray = hourlyArray
            guard let current = self.viewModel.resultRX.value?.current else {return}
            self.current = current
            let id = current.weather[0].id
            switch id {
            case 300...521:
                self.completeSceneRain.scaleMode = .aspectFill
                self.snowView.presentScene(self.completeSceneRain)
                self.snowView.isHidden = false
            case 600...622:
                self.completeSceneSnow.scaleMode = .aspectFill
                self.snowView.presentScene(self.completeSceneSnow)
                self.snowView.isHidden = false
            default:
                self.snowView.isHidden = true
                self.setImageView()
            }
            self.myTableView.reloadData()
            self.collectionView.reloadData()
        }).disposed(by: disposeBag)
        
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController else {return}
        controller.modalPresentationStyle = .popover
        controller.cities = viewModel.cities
        controller.completion = {city in
            self.viewModel.getWeather(city: city)
        }
        self.present(controller, animated: true, completion: nil)
    }
}


