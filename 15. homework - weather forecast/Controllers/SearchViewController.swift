

import UIKit

class SearchViewController: UIViewController, UISearchResultsUpdating {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var completion: ((City) -> Void)?
    var cities = [City]()
    var filteredCities: [City] = []
    let searchController = UISearchController(searchResultsController: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setImageView()
        imageView.contentMode = .center
        let image = UIImage()
        searchController.searchBar.backgroundImage = image
        searchController.searchBar.tintColor = .darkGray
        searchController.searchBar.placeholder = "Search City".localized
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.showsCancelButton = true
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    func setImageView (){
        let imageViewDay = UIImage(named: "background")
        let imageViewNight = UIImage(named: "cloud22")
        let hour = Calendar.current.component(.hour, from: Date())
        switch hour {
        case 1...6:
            imageView.image = imageViewNight
        case 7...18:
            imageView.image = imageViewDay
        default:
            imageView.image = imageViewNight
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
        filteredCities = cities.filter {
            let first =  $0.name.prefix(searchText.count)
            return first.lowercased() == searchText.lowercased()
        }
        tableView.reloadData()
    }
}


extension SearchViewController: UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        dismiss(animated: true, completion: nil)
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomSearchTableViewCell", for: indexPath) as? CustomSearchTableViewCell
        else {
            return UITableViewCell()
        }
        cell.configure(object: filteredCities[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = filteredCities[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        dismiss(animated: true, completion: {
            self.completion?(city)
        })
    }
}
