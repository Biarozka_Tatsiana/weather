

import UIKit

class SecondCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var windSpeedValueLabel: UILabel!
    @IBOutlet weak var feelsLikeValueLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    func configure (object: Current) {
        windSpeedLabel.text = "wind speed".localized
        feelsLikeLabel.text = "feels like".localized
        windSpeedValueLabel.text = String(object.wind_speed) + " m/s".localized
        feelsLikeValueLabel.text = String(Int(object.feels_like.rounded())) + "°"
        contentView.backgroundColor = .clear
    }
}
