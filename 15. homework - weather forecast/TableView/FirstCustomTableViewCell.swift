
import UIKit

class FirstCustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var sunriseDateLabel: UILabel!
    @IBOutlet weak var sunsetDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
 
    }

    func configure (object: Current) {
        sunriseLabel.text = "sunrise".localized
        sunsetLabel.text = "sunset".localized
        sunriseDateLabel.text = getDateSun(current: object.sunrise)
        sunsetDateLabel.text = getDateSun(current: object.sunset)
    }

    func getDateSun (current: Int) -> String {
        
        let today = Date(timeIntervalSince1970: Double(current))
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let string = formatter.string(from: today)
        return string
    }
}
