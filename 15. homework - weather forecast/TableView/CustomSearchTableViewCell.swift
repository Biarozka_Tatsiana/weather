

import UIKit

class CustomSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var filteredCityLabel: UILabel!
    
    var filteredCities: [City] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    func configure (object: City ) {
        filteredCityLabel.text = object.name
    }
}
