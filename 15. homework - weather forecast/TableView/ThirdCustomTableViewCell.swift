

import UIKit

class ThirdCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var pressureValueLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var humidityValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func configure (object: Current) {
        pressureLabel.text = "pressure".localized
        humidityLabel.text = "humidity".localized
        pressureValueLabel.text = String(object.pressure) + " hPa".localized
        humidityValueLabel.text = String(object.humidity) + " %"
    }
}
