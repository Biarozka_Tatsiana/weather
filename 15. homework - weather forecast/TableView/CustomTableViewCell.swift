

import UIKit



class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dayNameLabel: UILabel!
    @IBOutlet weak var dailyTempLabel: UILabel!
    @IBOutlet weak var nightTempLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    
    var dailyArray = [Daily]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configure(object: Daily) {
        
        dayNameLabel.text = getWeekDay(daily: object)
        dailyTempLabel.text = String(Int(object.temp.day.rounded()))
        nightTempLabel.text = String(Int(object.temp.night.rounded()))
        weatherImageView.image = UIImage(named: object.weather[0].icon)
        contentView.backgroundColor = .clear
    }
    
    func getWeekDay (daily: Daily) -> String {
        
        let date = Date(timeIntervalSince1970: Double(daily.dt))
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        var string = formatter.string(from: date).capitalized
        if let index = string.firstIndex(of: ",") {
            string = String(string.prefix(upTo: index))
        }
        return string
    }
}
