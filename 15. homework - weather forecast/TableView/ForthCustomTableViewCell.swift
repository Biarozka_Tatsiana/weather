

import UIKit

class ForthCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var cloudsLabel: UILabel!
    @IBOutlet weak var cloudsValueLabel: UILabel!
    @IBOutlet weak var uVLabel: UILabel!
    @IBOutlet weak var uvValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func configure (object: Current) {
        cloudsLabel.text = "cloudiness".localized
        uVLabel.text = "uv index".localized
        cloudsValueLabel.text = String(object.clouds) + " %"
        uvValueLabel.text = String(Int(object.uvi.rounded()))
    }
}
