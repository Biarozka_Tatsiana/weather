

import UIKit

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return dailyArray.count
        } else {
            return rowSectionFirstIndex
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell
            else {
                return UITableViewCell()
            }
            
            cell.configure(object: dailyArray[indexPath.row])
            return cell
        } else {
            
            if indexPath.row == 0 {
                guard let cellCurrentSun = tableView.dequeueReusableCell(withIdentifier: "FirstCustomTableViewCell", for: indexPath) as? FirstCustomTableViewCell else {
                    return UITableViewCell() }
                if let current = current {
                    cellCurrentSun.configure(object: current)
                }
                return cellCurrentSun
            } else if indexPath.row == 1 {
                guard let cellCurrentWindFeels = tableView.dequeueReusableCell(withIdentifier: "SecondCustomTableViewCell", for: indexPath) as? SecondCustomTableViewCell else {
                    return UITableViewCell () }
                if let current = current {
                    cellCurrentWindFeels.configure(object: current)
                }
                return cellCurrentWindFeels
            } else if indexPath.row == 2 {
                guard let cellCurrentPressureHumidity = tableView.dequeueReusableCell(withIdentifier: "ThirdCustomTableViewCell", for: indexPath) as? ThirdCustomTableViewCell else {
                    return UITableViewCell ()}
                if let current = current {
                    cellCurrentPressureHumidity.configure(object: current)
                }
                return cellCurrentPressureHumidity
            }
        }
        
        guard let cellCurrentCloudsUV = tableView.dequeueReusableCell(withIdentifier: "ForthCustomTableViewCell", for: indexPath) as? ForthCustomTableViewCell else {
            return UITableViewCell ()}
        if let current = current {
            cellCurrentCloudsUV.configure(object: current)
        }
        return cellCurrentCloudsUV
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        numberOfSections
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1 {
            return heightSecondSection
        }
        return heightRestSection
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
