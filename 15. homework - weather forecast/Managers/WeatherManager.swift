
import Foundation

class WeatherManager {
    
    static let shared = WeatherManager ()
    
    let baseUrl = "https://api.openweathermap.org/data/2.5"
    let keyAPI = "966c4e729aa70eb40d4d439514f034b8"
    var oneCall = ""
    var latitudeURL = ""
    var longitudeURL = ""
    
    enum Location: String {
        case en = "en"
        case ru = "ru"
    }
    
    func buildURL(locale: Location) -> String {
        
        switch locale {
        
        case .en:
            oneCall = "/onecall?lat=" + latitudeURL + "&lon=" + longitudeURL + "&units=metric&lang=en&exclude=minutely,alerts&appid=" + keyAPI
            
        case .ru:
            oneCall = "/onecall?lat=" + latitudeURL + "&lon=" + longitudeURL + "&units=metric&lang=ru&exclude=minutely,alerts&appid=" + keyAPI
        }
        return baseUrl + oneCall
    }
    
    func setLatitude(_ latitude: String) {
        latitudeURL = latitude
    }
    
    func setLatitude(_ latitude: Double) {
        setLatitude(String(latitude))
    }
    
    func setLongitude(_ longitude: String) {
        longitudeURL = longitude
    }
    
    func setLongitude(_ longitude: Double) {
        setLongitude(String(longitude))
    }
    
    func getWeather(success: @escaping (Result) -> (), onError: @escaping (String) -> ()) {
        
        var locale = Location.en
        if Locale.current.languageCode == Location.ru.rawValue {
            locale = Location.ru
        }
        
        guard let url = URL(string: buildURL(locale: locale)) else {
            
            onError("Error")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            
            DispatchQueue.main.async {
                
                if error == nil, let data = data {
                    do {
                        let object = try JSONDecoder().decode(Result.self, from: data)
                        success(object)
                    }
                    catch {
                        onError(error.localizedDescription)
                    }
                }
            }
        }
        task.resume()
    }
}
