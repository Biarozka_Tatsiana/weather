

import Foundation

class JSONParsingManager {
    
    static let shared = JSONParsingManager()
    private init() {}
    
    func parseJson() -> [City]{
        guard let path = Bundle.main.path(forResource: "city", ofType: "json") else {
            return []}
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let cities = try JSONDecoder().decode([City].self, from: data)
            return cities
        } catch {
            print(error.localizedDescription)
            return []
        }
    }
}
