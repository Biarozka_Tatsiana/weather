

import Foundation
import UIKit
import CoreLocation
import RxCocoa
import RxSwift

class ViewModel: NSObject, CLLocationManagerDelegate  {
    
    var resultRX = BehaviorRelay<Result?>(value: nil)
    var currentLocation: CLLocation?
    private let locationManager = CLLocationManager()
    var cities = [City]()
    var cityCurrent = ""
    var dailyArray = [Daily]()
    var hourlyArray = [Hourly]()
    var current: Current?
    
    override init() {
        self.cities = JSONParsingManager.shared.parseJson()
        super.init()
    }

    func getWeather (city: City?) {
        if let city = city {
            WeatherManager.shared.setLatitude(city.coord.lat)
            WeatherManager.shared.setLongitude(city.coord.lon)
            WeatherManager.shared.getWeather { (result) in
                self.cityCurrent = city.name
                self.resultRX.accept(result)
            } onError: { (error) in
                print(error)
            }
        } else {
            getLocation()
        }
    }
    
    func getLocation () {
        
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        
        if let location =  locations.last {
            self.currentLocation = location
            let latitude: Double = self.currentLocation?.coordinate.latitude ?? 0.0
            let longitude: Double = self.currentLocation?.coordinate.longitude ?? 0.0
            
            WeatherManager.shared.setLatitude(latitude)
            WeatherManager.shared.setLongitude(longitude)
            
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                if let error = error {
                    print(error.localizedDescription)
                }
                if let placemarks = placemarks {
                    if placemarks.count > 0 {
                        let placemark = placemarks[0]
                        if let cityLocal = placemark.locality {
                            for city in self.cities {
                                if city.name == cityLocal {
                                    self.cityCurrent = city.name
                                }
                            }
                        }
                    }
                }
                WeatherManager.shared.getWeather { (result) in
                    self.resultRX.accept(result)
                } onError: { (error) in
                    print(error)
                }
            }
        }
    }
}

