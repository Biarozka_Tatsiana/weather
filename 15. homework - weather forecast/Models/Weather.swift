

import Foundation

class Result: Decodable {
    var lat: Double
    var lon: Double
    var timezone: String
    var current: Current 
    var hourly: [Hourly]
    var daily: [Daily]
}

class Current: Decodable {
    var dt: Int
    var sunrise: Int
    var sunset: Int
    var temp: Double
    var feels_like: Double
    var pressure: Int
    var humidity: Int
    var dew_point: Double
    var uvi: Double
    var clouds: Int
    var visibility: Int
    var wind_speed: Double
    var wind_deg: Int
    var weather: [Weather]
}

class Weather: Decodable {
    var id: Int
    var main: String
    var description: String
    var icon: String
}

class Hourly: Decodable {
    var dt: Int
    var temp: Double
    var feels_like: Double
    var pressure: Int
    var humidity: Int
    var dew_point: Double
    var clouds: Int
    var visibility: Int
    var wind_speed: Double
    var wind_deg: Int
    var weather: [Weather]
}

class Daily: Decodable {
    var dt: Int
    var sunrise: Int
    var sunset: Int
    var temp: Temperature
    var feels_like: Feels_Like
    var pressure: Int
    var humidity: Int
    var dew_point: Double
    var wind_speed: Double
    var wind_deg: Int
    var weather: [Weather]
    var uvi: Double
    var clouds: Int
}

class Temperature: Decodable {
    var day: Double
    var min: Double
    var max: Double
    var night: Double
    var eve: Double
    var morn: Double
}

class Feels_Like: Decodable {
    var day: Double
    var night: Double
    var eve: Double
    var morn: Double
}
